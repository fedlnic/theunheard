﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

		if (Input.GetKey ("w"))
			transform.Translate (0, 0, 10 * Time.deltaTime);
		
		if (Input.GetKey ("s"))
			transform.Translate (0, 0, -10 * Time.deltaTime);
		
		if (Input.GetKey ("a"))
			transform.Translate (-10 * Time.deltaTime, 0, 0 );
		
		if (Input.GetKey ("d"))
			transform.Translate (10 * Time.deltaTime, 0, 0);
		
	}
}
